<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AkademikFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->method() == 'PUT' ? $this->siswa : 'NULL';
        return [
            'tahun' => 'required|unique:akademik,tahun,'.$id.',id,semester,'.$this->request->get('semester'),
            'semester' => 'required|in:ganjil,genap'
        ];
    }

    public function messages()
    {
        return [
            'tahun.required' => 'Tahun Ajaran harus diisi',
            'tahun.unique' => 'Tahun Ajaran dan semester sudah terdaftar',
            'semester.required' => 'Semester harus diisi',
            'semester.in' => 'Semester harus berisi ganjil atau genap',
        ];
    }
}
