<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Akademik;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use \App\Http\Requests\AkademikFormRequest;

class AkademikController extends Controller
{
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            return Datatables::of(Akademik::orderBy('id','desc'))
                ->editColumn('semester', function($akademik) {
                    return strtoupper($akademik->semester);
                })
                ->editColumn('aktif', function($akademik) {
                    return $akademik->aktif ? 'Y' : 'T';
                })
                ->addColumn('action', function($akademik) {
                    return view('vendor.datatables._action_edit_delete', [
                        'edit_url' => route('akademik.edit', $akademik->id),
                        'model' => $akademik,
                        'form_url' => route('akademik.destroy', $akademik->id),
                        'confirm_message' => 'Anda yakin ingin menghapus '.$akademik->deskripsi.'?'
                    ]);
                })
                ->make(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data'=>'id','name'=>'id','title'=>'ID','class'=>'all'])
            ->addColumn(['data'=>'tahun_pelajaran','name'=>'tahun_pelajaran','title'=>'Tahun Pelajaran','class'=>'all'])
            ->addColumn(['data'=>'semester','name'=>'semester','title'=>'Semester','class'=>'all'])
            ->addColumn(['data'=>'aktif','name'=>'aktif','title'=>'Aktif','class'=>'all'])
            ->addColumn(['data'=>'action','name'=>'action','title'=>'Action']);

        return view('akademik.index')->with(compact('html'));
    }

    public function create()
    {
        $years = [];
        for ($year=date('Y')-1; $year <= date('Y'); $year++) $years[$year] = $year.'/'.($year+1);

        return view('akademik.create')->with(compact('years'));
    }

    public function edit($id)
    {
        $akademik = Akademik::findOrFail($id);
        return view('akademik.edit')->with(compact('akademik'));
    }

    public function store(AkademikFormRequest $request)
    {
        $akademik = Akademik::create($request->all());

        $request->session()->flash('flash_notification', [
            'level' => 'success',
            'message' => 'Berhasil menyimpan data'
        ]);

        return redirect()->route('akademik.index');
    }

    public function update(AkademikFormRequest $request, $id)
    {
        $akademik = Akademik::findOrFail($id);
        $akademik->update($request->all());

        $request->session()->flash('flash_notification', [
            'level' => 'success',
            'message' => 'Berhasil menyimpan data'
        ]);

        return redirect()->route('akademik.index');
    }

    public function destroy(Request $request, $id)
    {
        if (!Akademik::destroy($id)) return redirect()->back();

        $request->session()->flash('flash_notification', [
            'level' => 'success',
            'message' => 'Data berhasil dihapus'
        ]);

        return redirect()->route('akademik.index');
    }
}
