<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class KelasController extends Controller
{

    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            return Datatables::of(Kelas::all())
                ->addColumn('action', function($kelas) {
                    return view('vendor.datatables._action_edit_delete', [
                        'edit_url' => route('kelas.edit', $kelas->id),
                        'model' => $kelas,
                        'form_url' => route('kelas.destroy', $kelas->id),
                        'confirm_message' => 'Anda yakin ingin menghapus '.$kelas->deskripsi.'?'
                    ]);
                })->make(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data'=>'id','name'=>'id','title'=>'ID','class'=>'all'])
            ->addColumn(['data'=>'deskripsi','name'=>'deskripsi','title'=>'Deskripsi','class'=>'all'])
            ->addColumn(['data'=>'action','name'=>'action','title'=>'Action']);

        return view('kelas.index')->with(compact('html'));
    }

    public function create()
    {
        return view('kelas.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'deskripsi' => 'required|unique:kelas,deskripsi',
        ], [
            'deskripsi.required' => 'Kelas harus diisi'
        ]);

        $kelas = Kelas::create($request->all());

        $request->session()->flash('flash_notification', [
            'level' => 'success',
            'message' => 'Berhasil menyimpan '. $kelas->deskripsi
        ]);

        return redirect()->route('kelas.index');
    }

    public function edit($id)
    {
        $kelas = Kelas::findOrFail($id);
        return view('kelas.edit')->with(compact('kelas'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'deskripsi' => 'required|unique:kelas,deskripsi,'.$id,
        ], [
            'deskripsi.required' => 'Kelas harus diisi'
        ]);

        $kelas = Kelas::findOrFail($id);
        $kelas->update($request->all());

        $request->session()->flash('flash_notification', [
            'level' => 'success',
            'message' => 'Berhasil menyimpan '. $kelas->deskripsi
        ]);

        return redirect()->route('kelas.index');
    }

    public function destroy(Request $request, $id)
    {
        Kelas::destroy($id);

        $request->session()->flash('flash_notification', [
            'level' => 'success',
            'message' => 'Data kelas berhasil dihapus'
        ]);

        return redirect()->route('kelas.index');
    }
}
