<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $counter = [
            'kelas' => \App\Models\Kelas::count(),
            'siswa' => \App\Models\Siswa::aktif()->count(),
            'tahunAjaran' => \App\Models\Akademik::count(),
            'presensi' => \App\Models\Presensi::count(),
        ];

        return view('dashboard.index')->with(compact('counter'));
    }
}
