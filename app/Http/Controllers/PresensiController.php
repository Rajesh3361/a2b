<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Presensi;
use App\Models\Siswa;
use App\Models\Akademik;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PresensiController extends Controller
{
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $presensi = Presensi::with([
                'siswa' => function($query) {
                    $query->select('id','nis','nama');
                },
                'kelas'])->orderBy('created_at','desc');

            return Datatables::of($presensi)
                ->editColumn('tanggal', function($presensi) {
                    return $presensi->tanggal ? $presensi->tanggal->format('d/m/Y') : '';
                })
                ->editColumn('status', function($presensi) {
                    return '<div class="presensi-group btn-group" data-toggle="buttons">
                        <label class="btn btn-default '.($presensi->status =='H' ? 'btn-success active' : '').'">
                          <input type="radio" name="presensi" data-value="H" data-presensi="'.$presensi->id.'"> Hadir
                        </label>
                        <label class="btn btn-default '.($presensi->status =='S' ? 'btn-success active' : '').'">
                          <input type="radio" name="presensi" data-value="S" data-presensi="'.$presensi->id.'"> Sakit
                        </label>
                        <label class="btn btn-default '.($presensi->status =='I' ? 'btn-success active' : '').'">
                          <input type="radio" name="presensi" data-value="I" data-presensi="'.$presensi->id.'"> Izin
                        </label>
                        <label class="btn btn-default '.($presensi->status =='A' ? 'btn-success active' : '').'">
                          <input type="radio" name="presensi" data-value="A" data-presensi="'.$presensi->id.'"> Alpha
                        </label>
                      </div>';
                })
                ->make(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data'=>'tanggal','name'=>'tanggal','title'=>'Tanggal','class'=>'all'])
            ->addColumn(['data'=>'siswa.nis','name'=>'siswa.nis','title'=>'No Induk','class'=>'all'])
            ->addColumn(['data'=>'siswa.nama','name'=>'siswa.nama','title'=>'Nama Lengkap','class'=>'all','width'=>'35%'])
            ->addColumn(['data'=>'kelas.deskripsi','name'=>'kelas.deskripsi','title'=>'Kelas','class'=>'all'])
            ->addColumn(['data'=>'status','name'=>'status','title'=>'Absen','class'=>'all']);

        return view('presensi.index')->with(compact('html'));
    }

    public function laporan()
    {
        return view("presensi.laporan");
    }

    public function scanQrcode(Request $request)
    {
        if ($request->ajax()) {
            $qrcode = $request->get('qrcode');
            $tanggal = date('Y-m-d');
            $jam = date('H:i:s');
            $akademik_id = Akademik::whereAktif(1)->orderBy('created_at','desc')->first()->id;

            if ($siswa = Siswa::where('kode_qr', $qrcode)->first()) {
                if (!$siswa->presensi()->where('tanggal', $tanggal)->count()) {
                    $presensi = $siswa->presensi()->save(new Presensi([
                        'tanggal' => $tanggal,
                        'kelas_id' => $siswa->kelas_id,
                        'akademik_id' => $akademik_id,
                        'status' => 'H',
                        'jam_masuk' => $jam,
                        'jam_pulang' => '00:00:00',
                    ]));
                    return json_encode(['status'=>'success', 'data'=>['nis'=>$siswa->nis, 'nama'=>$siswa->nama, 'kelas'=>$siswa->kelas->deskripsi, 'jam'=>$jam]]);
                }
                return json_encode(['status'=>'recorded']);
            }
            return json_encode(['status'=>'failed']);
        }

        $presensi = Presensi::with([
            'siswa' => function($query) {
                $query->select('id','nis','nama');
            },
            'kelas'])
            ->where('tanggal',date('Y-m-d'))
            ->where('status','H')
            ->orderBy('created_at','desc')->get();
        return view('presensi.scanqrcode')->with(compact('presensi'));
    }

    public function create()
    {
        $siswa = Siswa::aktif()->with(['presensi'=>function($query) {
            return $query->where('tanggal',date('Y-m-d'));
        }])->select('id','nama')->get()->filter(function($siswa) {
            return $siswa->presensi->count() == 0;
        })->pluck('nama','id')->toArray();

        return view('presensi.create')->with(compact('siswa'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'siswa_id' => 'required',
            'status' => 'required|in:H,I,S,A'
        ], [
            'siswa_id.required' => 'Data Siswa harus diisi',
            'status.required' => 'Status harus diisi',
            'status.in' => 'Pilihan status hanya Hadir, Izin, Sakit dan Alpha'
        ]);

        $siswa_id = $request->get('siswa_id');
        $status = $request->get('status');
        $tanggal = date('Y-m-d');
        $jam = date('H:i:s');
        $akademik_id = Akademik::whereAktif(1)->orderBy('created_at','desc')->first()->id;

        if ($siswa = Siswa::findOrFail($siswa_id)) {
            $siswa->presensi()->firstOrCreate([
                'tanggal' => $tanggal,
                'kelas_id' => $siswa->kelas_id,
                'akademik_id' => $akademik_id,
                'status' => $status,
                'jam_masuk' => $jam,
                'jam_pulang' => '00:00:00',
            ]);

            $request->session()->flash('flash_notification', [
                'level' => 'success',
                'message' => 'Berhasil menyimpan data presensi'
            ]);
        } else {
            $request->session()->flash('flash_notification', [
                'level' => 'danger',
                'message' => 'Data siswa tidak ditemukan'
            ]);
        }

        return redirect()->route('presensi.index');
    }

    public function update(Request $request, $id)
    {
        $presensi = Presensi::findOrFail($id);
        $updated = $presensi->update([
            'status' => $request->get('status')
        ]);

        if ($updated) {
            return response()->json([
                'status' => 'success',
            ]);   
        } else {
            return response()->json([
                'status' => 'failed',
            ]);   
        }
    }

}
