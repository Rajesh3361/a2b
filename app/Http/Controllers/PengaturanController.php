<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengaturan;
use Illuminate\Support\Facades\Cache;

class PengaturanController extends Controller
{
    public function index()
    {
        $pengaturan = Pengaturan::first();
        return view('pengaturan.index')->with(compact('pengaturan'));
    }

    public function update(Request $request)
    {
        $pengaturan = Pengaturan::first();
        $pengaturan->update($request->all());

        Cache::forget('pengaturan.cache');

        $request->session()->flash('flash_notification', [
            'level' => 'success',
            'message' => 'Berhasil menyimpan data pengaturan'
        ]);

        return redirect()->route("pengaturan.index");
    }
}
