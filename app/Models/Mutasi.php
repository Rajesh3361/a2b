<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mutasi extends Model
{
    protected $table = 'mutasi';
    protected $fillable = ['tanggal','status_mutasi','keterangan'];
    protected $date = ['tanggal'];
}
