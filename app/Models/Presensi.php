<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Presensi extends Model
{
    protected $table = 'presensi';
    protected $fillable = ['tanggal','siswa_id','kelas_id','akademik_id','status',
        'jam_masuk','jam_pulang','keterangan'];
    protected $dates = ['tanggal'];

    public function siswa()
    {
        return $this->belongsTo('App\Models\Siswa');
    }

    public function kelas()
    {
        return $this->belongsTo('App\Models\Kelas');
    }

    public function akademik()
    {
        return $this->belongsTo('App\Models\Akademik');
    }
}
