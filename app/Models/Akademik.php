<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Akademik extends Model
{
    protected $table = 'akademik';
    protected $fillable = ['tahun','semester'];

    public static function boot()
    {
        parent::boot();
        self::deleting(function($akademik) {
            if ($akademik->presensi()->count() > 0) {
                Session::flash('flash_notification', [
                    'level' => 'danger',
                    'message' => 'Tahun pelajaran tidak dapat dihapus karena masih ada data presensi.'
                ]);
                return false;
            }
        });

        self::creating(function($akademik) {
            $akademik->tahun_pelajaran = $akademik->tahun.'/'.($akademik->tahun+1);
        });

        self::updating(function($akademik) {
            $akademik->tahun_pelajaran = $akademik->tahun.'/'.($akademik->tahun+1);
        });

    }

    public function presensi()
    {
        return $this->hasMany('App\Models\Presensi');
    }
}
