<?php 

namespace App\Helpers;
use \App\Models\Pengaturan;
use Illuminate\Support\Facades\Cache;

class Helper 
{
    public static function pengaturan()
    {
        $minutes = "60";
        $value = Cache::remember('pengaturan.cache', $minutes, function()
        {
            return Pengaturan::first();
        });

        return $value;
        
    }
}