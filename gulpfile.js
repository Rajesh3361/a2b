const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix
    .copy('resources/assets/global/plugins/font-awesome/fonts','public/build/fonts')
    .copy('resources/assets/global/plugins/simple-line-icons/fonts','public/build/css/fonts')
    .copy('resources/assets/layouts/layout/img','public/build/img')
    .copy('resources/assets/layouts/layout4/img','public/img')
    .copy('resources/assets/audio','public/audio')
    .copy('resources/assets/pages/img/login','public/img/login')
    .copy('resources/assets/pages/img/background','public/img/background')
    .copy('resources/assets/global/plugins/uniform/images/','public/build/images')
    .copy('resources/assets/global/plugins/datatables/images/','public/build/plugins/datatables/images')
    .styles([
        'resources/assets/global/plugins/datatables/datatables.min.css',
        'resources/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
        'resources/assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'resources/assets/global/plugins/bootstrap/css/bootstrap.min.css',
        'resources/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
        'resources/assets/global/plugins/uniform/css/uniform.default.css',
        'resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'node_modules/sweetalert/dist/sweetalert.css',
        'resources/assets/global/plugins/select2/css/select2.min.css',
        'resources/assets/global/plugins/select2/css/select2-bootstrap.min.css',
        'resources/assets/global/css/components-md.min.css',
        'resources/assets/global/css/plugins-md.min.css',
        'resources/assets/layouts/layout4/css/layout.min.css',
        'resources/assets/layouts/layout4/css/themes/default.min.css',
        'resources/assets/pages/css/profile.min.css',
        'resources/assets/layouts/layout4/css/custom.min.css',
        'resources/assets/custom/css/app.css',
    ], 'public/css/all.css','./')
    .styles([
        'resources/assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'resources/assets/global/plugins/bootstrap/css/bootstrap.min.css',
        'resources/assets/global/plugins/uniform/css/uniform.default.css',
        'resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'resources/assets/global/css/components-md.min.css',
        'resources/assets/global/css/plugins-md.min.css',
        'resources/assets/pages/css/login-5.min.css',
        'resources/assets/custom/css/login.css',
    ], 'public/css/login.css','./')
    .scripts([   
        'resources/assets/global/plugins/jquery.min.js',
        'resources/assets/global/plugins/bootstrap/js/bootstrap.min.js',
        'resources/assets/global/scripts/datatable.js',
        'resources/assets/global/plugins/datatables/datatables.min.js',
        'resources/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',       
        'resources/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js', 
        'resources/assets/global/plugins/js.cookie.min.js',
        'resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
        'resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'resources/assets/global/plugins/jquery.blockui.min.js',
        'resources/assets/global/plugins/uniform/jquery.uniform.min.js',
        'resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'resources/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
        'resources/assets/global/plugins/jquery-validation/js/additional-methods.min.js',
        'node_modules/sweetalert/dist/sweetalert.min.js',
        'resources/assets/global/plugins/select2/js/select2.full.min.js',
        'resources/assets/global/scripts/app.min.js',
        'resources/assets/layouts/layout4/scripts/layout.min.js',
        'resources/assets/layouts/global/scripts/quick-sidebar.min.js',
        'resources/assets/global/plugins/html5-qrcode/jsqrcode-combined.min.js',
        'resources/assets/global/plugins/html5-qrcode/html5-qrcode.min.js',
        'node_modules/howler/dist/howler.min.js',
        'resources/assets/custom/js/app.js',
    ], 'public/js/all.js', './')
    .scripts([   
        'resources/assets/global/plugins/jquery.min.js',
        'resources/assets/global/plugins/bootstrap/js/bootstrap.min.js',
        'resources/assets/global/plugins/js.cookie.min.js',
        'resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
        'resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'resources/assets/global/plugins/jquery.blockui.min.js',
        'resources/assets/global/plugins/uniform/jquery.uniform.min.js',
        'resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'resources/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
        'resources/assets/global/plugins/jquery-validation/js/additional-methods.min.js',
        'resources/assets/global/plugins/backstretch/jquery.backstretch.min.js',
        'resources/assets/global/scripts/app.min.js',
        'resources/assets/pages/scripts/login-5.js',
        'resources/assets/custom/js/login.js',
    ], 'public/js/login.js', './')
    .version(['css/all.css','js/all.js','js/login.js','css/login.css']);
});