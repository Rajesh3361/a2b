<div class="btn-group">
  <a href="{{ $show_url }}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Detil</a>
  <button type="button" class="btn btn-xs btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="{{ $edit_url }}">Edit</a></li>
    <li>
        <a href="{{ $form_url }}" class="js-delete">Delete</a>
        {!! Form::model($model, ['url'=>$form_url, 'method'=>'delete', 'style'=>'display:none']) !!}
        {!! Form::close() !!}
    </li>

  </ul>
</div>