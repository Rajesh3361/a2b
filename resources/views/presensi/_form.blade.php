<div class="form-body">
    @include('layouts._flash')

    <div class="form-group {{ $errors->has('siswa_id') ? 'has-error' : '' }}"">
        {!! Form::label('siswa_id','Siswa',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            {!! Form::select('siswa_id', [''=>''] + $siswa, null, ['class'=>'form-control select2me']) !!}
            {!! $errors->first('siswa_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}"">
        {!! Form::label('status','Status',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            <div class="radio-list">
                <label class="radio-inline">
                    {{ Form::radio('status','H', null)}}  Hadir 
                </label>
                <label class="radio-inline">
                    {{ Form::radio('status','I', null)}}  Izin
                </label>
                <label class="radio-inline">
                    {{ Form::radio('status','S', null)}}  Sakit
                </label>
                <label class="radio-inline">
                    {{ Form::radio('status','A', null)}}  Alpha
                </label>
            </div>
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn green">Submit</button>
            <a href="{{ URL::previous() }}" type="button" class="btn default">Cancel</a>
        </div>
    </div>
</div>  