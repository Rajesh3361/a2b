@extends('layouts.login')

@section('content')

<h1> Admin Login</h1>
<p>Aplikasi Presensi {{ Helper::pengaturan()->nama_sekolah }}</p>
<form action="{{ url('/login') }}" class="login-form" method="post">

    {{ csrf_field() }}

    @if(count($errors))
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                {{ $error ."\n" }}
            @endforeach
        </div>
    @endif

    <div class="row">
        <div class="col-xs-6">
            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" 
            autocomplete="off" placeholder="Username" name="username" value="{{ old('username') }}" required autofocus /> 
        </div>
        <div class="col-xs-6">
            <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" 
            autocomplete="off" placeholder="Password" name="password" required autofocus /> 
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="rem-password">
                <p>Remember Me <input type="checkbox" class="rem-checkbox" name="remember" {{ old('remember') ? 'checked' : ''}} />
                </p>
            </div>
        </div>
        <div class="col-sm-8 text-right">
            <button class="btn blue" type="submit">Sign In</button>
        </div>
    </div>
</form>

@endsection