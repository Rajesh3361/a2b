<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{ Helper::pengaturan()->nama_aplikasi }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'gAbsensi') }}</title>
    
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ elixir('css/all.css') }}">
        <script>
            window.gAbsensi = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
        
        <link rel="shortcut icon" href="/favicon.ico" /> 
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        @include('layouts._header')

        <div class="page-container">
            <div class="page-sidebar-wrapper">
                @include('layouts._sidebar')
            </div>
            <div class="page-content-wrapper">
                @yield('content')
            </div>
        </div>

        @include('layouts._footer')
        
        <script src="{{ elixir('js/all.js') }}"></script>

        @yield('scripts')
    </body>
</html>