<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <div class="page-logo">
            <a href="{{ url('/') }}" class="logo-default">
                {{ Helper::pengaturan()->nama_aplikasi }}
            </a>
            <div class="menu-toggler sidebar-toggler">
            </div>
        </div>
        
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        
        @yield('page-actions')
        
        <div class="page-top">
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="separator hide"> </li>
                    @if(Auth::user())
                        <li class="dropdown dropdown-user dropdown-extended">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="username username-hide-on-mobile"> Hi,  {{ Auth::user()->name }} </span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="{{ route('pengaturan.index') }}"><i class="icon-user"></i> Pengaturan </a>
                                </li>
                                <li>
                                    <a href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit()"><i class="icon-key"></i> Keluar </a>
                                    <form action="{{ url('/logout') }}" id="logout-form" method="POST" style="display:none">{{ csrf_field() }}</form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"> </div>