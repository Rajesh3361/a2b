<div class="form-body">
    @include('layouts._flash')

    <div class="form-group {{ $errors->has('tahun') ? 'has-error' : '' }}"">
        {!! Form::label('tahun','Tahun Ajaran',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            {!! Form::select('tahun', $years, null, ['class'=>'form-control']) !!}
            {!! $errors->first('tahun', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('semester') ? 'has-error' : '' }}"">
        {!! Form::label('semester','Semester',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            {!! Form::select('semester', ['ganjil'=>'Ganjil','genap'=>'Genap'], null, ['class'=>'form-control']) !!}
            {!! $errors->first('semester', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn green">Submit</button>
            <a href="{{ URL::previous() }}" type="button" class="btn default">Cancel</a>
        </div>
    </div>
</div>  