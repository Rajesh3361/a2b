<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkademikTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akademik', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tahun_pelajaran');
            $table->integer('tahun');
            $table->enum('semester',['genap','ganjil']);
            $table->boolean('aktif')->default(true);
            $table->timestamps();

            $table->unique( array('tahun','semester') );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akademik');
    }
}
