<?php

use Illuminate\Database\Seeder;

class AkademikTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Akademik::create([
            'tahun_pelajaran'=>'2016/2017',
            'tahun' => 2016,
            'semester'=>'ganjil',
            'aktif' => true
        ]);
    }
}
