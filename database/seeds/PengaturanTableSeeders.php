<?php

use Illuminate\Database\Seeder;
use App\Models\Pengaturan;

class PengaturanTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pengaturan::create([
            'nama_aplikasi' => 'gAbsensi',
            'nama_sekolah' => 'SMK N Pondok Cabe'
        ]);
    }
}
