<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PresensiTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,12) as $bulan) {
            foreach (range(1,30) as $hari) {
                $number = rand(1,10);
                if ($number % 2 == 0) continue;
                foreach(App\Models\Siswa::all() as $siswa) {
                    $siswa->presensi()->create([
                        'tanggal' => '2016-'.str_pad($bulan,2,0,STR_PAD_LEFT).'-'.str_pad($hari,2,0,STR_PAD_LEFT),
                        'kelas_id' => $siswa->kelas_id,
                        'status' => ['I','S','A','H'][rand(0,3)],
                        'jam_masuk' => $faker->time('H:i:s', 'now'),
                        'jam_pulang' => $faker->time('H:i:s', 'now'),
                        'akademik_id' => 1
                    ]);
                }
            }
        }
    }
}
